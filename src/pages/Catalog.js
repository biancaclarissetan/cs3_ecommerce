
import Hero from '../components/Banner';
import SubjectCard from '../components/ProductCard';
import {Container} from 'react-bootstrap';
import {useEffect,useState} from 'react';


let info = {
	title: "SHOP ALL",
	tagline: "You are now viewing all store items"
}


export default function Products(){
     

	const [products, setProducts] = useState([]); 
	
	useEffect(()=>{


		fetch('https://stark-meadow-66994.herokuapp.com/products/shop').then(outcomeNgFetch=>outcomeNgFetch.json()).then(convertedData=>{
			setProducts(convertedData.map(subject=>{
				 
				return(
					<SubjectCard key={subject._id} infoNgKurso={subject}/>
				)
			}))
			}) 

	},[products])
	return(

		
		<div className="catalog productsBody p-5">
			
				<Container className="ml-3">
					<Hero className="productCatalogHero mt-5" kahitAno={info}/>
				</Container>
				<div className="productCatalog  row ">{products}</div>
				
		</div>
		
	

	);
}