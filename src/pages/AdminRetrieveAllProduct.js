
import Hero from '../components/Banner';
import SubjectCard from '../components/AdminProductCard';
import {Container} from 'react-bootstrap';
import {useEffect,useState} from 'react';

let info = {
	title: "ADMIN CATALOG VIEW",
	tagline: "You are now viewing all store items"
}


export default function AllProducts(){
     

	const [products, setProducts] = useState([]); 
	const [archive, setArchive] = useState([]);

	
	
	useEffect(()=>{
		fetch('https://stark-meadow-66994.herokuapp.com/products/allProducts',{
        headers:{
        Authorization: `Bearer ${localStorage.getItem('acessToken')}`
         }
        }).then(outcomeNgFetch=>outcomeNgFetch.json()).then(convertedData=>{
			setProducts(convertedData.map(subject=>{
				 
				return(
					<SubjectCard key={subject._id} infoNgKurso={subject}/>
				)
			}))
			}) 

	},[products])
	return(
		
		<div className="catalog productsBody p-5">
			
				<Container className="ml-3">
					<Hero className="productCatalogHero mt-5" kahitAno={info}/>
				</Container>
				<div className="productCatalog  row ">{products}</div>
				
		</div>
		
		
	

	);
}