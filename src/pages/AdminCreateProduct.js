
import {useState,useEffect} from 'react';
import {Form,Button,Container,Row,Nav,Card,Col} from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2'
import {Redirect, useHistory, NavLink} from 'react-router-dom';

const paraSaBanner ={
  title:"Create Your Own Account Here",
  tagline: "Think before you Click"
}


export default function RegisterProduct(){
  const history = useHistory();
  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice]= useState('');
  const [orderId, setOrderId] = useState('');
  const [isRegisterBtnActive,setIsRegisterBtnActive] = useState(false);
 
  const authenticate = (e) =>{
    e.preventDefault()

    fetch(`https://stark-meadow-66994.herokuapp.com/products/register`,{
      method:"POST",headers:{
        'Content-Type':'application/json',
        Authorization: `Bearer ${localStorage.getItem('acessToken')}`
      },body: JSON.stringify({
        productName: productName,
        description: description,
        price: price,
        orderId: orderId
      })
    })
    .then(res=>res.json())
    .then(information=>{
   console.log(information)
      if(information===true){
       /* localStorage.setItem('acessToken', information.acessToken)*/
        Swal.fire({
          title: "Product Registration Success" ,
          icon: "success"
        })
       
      }else{
        Swal.fire({
          title: "Product Registration Failed" ,
          icon: "error"
          
        })
      }
    })
  }

  useEffect(()=>{
    if(productName!=="" && description !=="" && price !="" && orderId!=""){
      setIsRegisterBtnActive(true)
    }else{
      setIsRegisterBtnActive(false)
    }
  },[productName,description,price,orderId])


  

 

  return( <div className="loginpage container-fluid">
          <div className="row no-gutter">      
              <div className="col-md-6 d-none d-md-flex" 
              style={{backgroundImage: `url(catalog3.jpg)`, backgroundRepeat: 'no-repeat',backgroundSize: 'cover', backgroundPosition:'center center'}} >
              </div>
         
              <div className="col-md-6">
                  <div className="login d-flex align-items-center py-5">
                      <Container className="container">
                          <Row className="row">
                              <div className="col-lg-10 col-xl-7 mx-auto">
                                  <h3 className="display-4">UPLOAD PRODUCT</h3>
                                
                                  <Form className="formBody" onSubmit={e=>authenticate(e)}  >


                                      <Form.Group as={Row} className="mb-3" controlId="productName">  
                                          
                                          <Col sm="9">
                                            <Form.Control type="text" placeholder="Insert Product Name Here" value={productName} onChange={event=>setProductName(event.target.value)} required></Form.Control>
                                          </Col>
                                      </Form.Group>

                                      <Form.Group as={Row} controlId="description"> 
                                       
                                        <Col sm="9">
                                          <Form.Control type="text" placeholder="Insert Product Descripton Here" value={description} onChange={event=>setDescription(event.target.value)}required></Form.Control>
                                        </Col>
                                      </Form.Group>
                                              
                                      <Form.Group as={Row} controlId="price"> 
                                        
                                         <Col sm="9">
                                          <Form.Control type="number" placeholder="Insert Product Price" value={price} onChange={event=>setPrice(event.target.value)} required></Form.Control>
                                         </Col>
                                      </Form.Group>

                                      <Form.Group as={Row} controlId="orderId"> 
                                       
                                         <Col sm="9">
                                          <Form.Control type="number" placeholder="Insert Order Id here" value={orderId} onChange={event=>setOrderId(event.target.value)} required></Form.Control>
                                         </Col>
                                      </Form.Group>
                                      

                                     {isRegisterBtnActive ?
                                      <Button type="submit" id="submitBtn" variant="warning" className="btn">CREATE PRODUCT</Button>
                                      :
                                      <Button type="submit" id="submitBtn" variant="danger" className="btn" disabled>CREATE PRODUCT</Button>
                                      }

                                      
                                  </Form>
                              </div>
                          </Row>
                      </Container>

                  </div>
              </div>

          </div>
      </div>
 
   

    
  
  )
}
