

import Hero from '../components/Banner';
import SubjectCard from '../components/ProductCard';
import {Container,Fragment,Row,Col,Card,Nav} from 'react-bootstrap';
import {useEffect,useState} from 'react';
import {Route,Switch,BrowserRouter as Router}from 'react-router-dom'
import {/*Link, */NavLink} from 'react-router-dom'


   
export default function SingleProduct(props){

	const id = props.match.params.id



	const [product, setProduct] = useState([]); 

	useEffect(()=>{
		fetch(`https://stark-meadow-66994.herokuapp.com/products/${id}`).then(outcomeNgFetch=>outcomeNgFetch.json()).then(convertedData=>{console.log(convertedData)//as a checker
			setProduct(convertedData)
			}) 

	},[product])

	let info = {
	title: "SINGLE PRODUCT",
	tagline: "You are now viewing all store items"
		}
	return(
		<div  className="catalog productsBody p-5">
		<Container>
		<div className="singleproduct">
	      <div className="page-wrapper p-t-5">
	          <div className="wrapper wrapper--w790">
	            <div className="registerForm">
	              <Card className="card">
				                <Row className="row">
				                    <Col className="col-md-6">
				                        <div className="images p-3">
				                            <div className="text-center p-4"> <img id="main-image" src={product._id + '.jpg'} width="250" /> 
				                            </div>
				                          {/*  <div className="thumbnail text-center"> 
				                             </div>*/}
				                        </div>
				                    </Col>
				                    <Col className="col-md-6">
				                        <div className="product p-4">
				                            <div className="d-flex justify-content-between align-items-center">
				                                <div className="d-flex align-items-center">
				                                	<Nav.Link style={{color: '#75654C', textDecoration: 'underline'}} as={NavLink} to={`/shop`}>BACK</Nav.Link>
				                                </div>
				                            </div>
				                            <div className="mt-4 mb-3"> <span className="text-uppercase text-muted brand">La Vida Wear</span>
				                                <h5 className="text-uppercase">{product.productName}</h5>
				                                <div className="price d-flex flex-row align-items-center"> <span className="act-price">Php {product.price}</span>
				                                    
				                                </div>
				                            </div>
				                            <p className="about">{product.description}</p>
				                          
				                            <div className="cart mt-4 align-items-center"> <button className="btn btn-danger text-uppercase mr-2 px-4">Add to cart</button>
				                            </div>
				                        </div>
				                    </Col>
				                </Row>
				            </Card>
	            </div>    
	          </div>
	      </div>
	    </div>
	    </Container>
   		</div>
	

	);
}