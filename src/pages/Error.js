import Hero from '../components/Banner'
import {Container} from 'react-bootstrap'

const heroKotoh ={
	title:"404 Page Not Found",
	tagline: "You are trying to access a non-existing page"
}

export default function Error(){
	return(
		<Container className="body">
			<Hero kahitAno={heroKotoh}/>
		</Container>
	)
}