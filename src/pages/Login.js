
import {NavLink} from 'react-router-dom'
import UserContext from '../UserContext';
import {useState, useEffect, useContext} from 'react';
import {Form,Button,Container,Row,Nav,Card,Col,Carousel} from 'react-bootstrap'
import {Redirect} from 'react-router-dom';
import Hero from '../components/Banner'
import Swal from 'sweetalert2'

const paraSaBanner ={
  title:"Create Your Own Account Here",
  tagline: "Think before you Click"
}



export default function Login(){

  const [email, setEmail] = useState('');
  const [password, setPassword] =useState('');
  const [isActive, setIsActive] = useState(false)
  const{user, setUser} = useContext(UserContext)


  const authenticate = (e) =>{
    e.preventDefault()

    fetch(`https://stark-meadow-66994.herokuapp.com/users/login`,{
      method:"POST",headers:{
        'Content-Type':'application/json'
      },body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res=>res.json())
    .then(information=>{
     

      if(typeof information.acessToken !=="undefined"){
        localStorage.setItem('acessToken', information.acessToken)
        Swal.fire({
          title: "Login successful" ,
          icon: "success",
          text: "Welcome to the App!"
        })
        retrieveUserDetails(information.acessToken);
      }else{
        Swal.fire({
          title: "Authentication Failed" ,
          icon: "error",
          text: "Check your login details and try again!"
        })
      }
    })
  }

  const retrieveUserDetails = (token) => {
    fetch(`https://stark-meadow-66994.herokuapp.com/users/details`,{
      headers:{
        Authorization: `Bearer ${token}`
      }
    }).then(resultOfPromise => resultOfPromise.json()).then(convertedResult=>{
      
      setUser({
        id: convertedResult._id,
        isAdmin: convertedResult.isAdmin
      })
    })
  }

  useEffect(()=>{
    if(email!=="" && password !==""){
      setIsActive(true)
    }else{
      setIsActive(false)
    }
  },[email,password])


 

  return(
    
    (user.id)? 
    (user.isAdmin===true)? 
    <Redirect to="/adminhome"/>
    :
     <Redirect to="/shop"/>
     :
   
      <div className="loginpage container-fluid">
          <div className="row no-gutter">      
              <div className="col-md-6 d-none d-md-flex bg-image" style={{backgroundImage: `url(catalog4.jpg)`, backgroundRepeat: 'no-repeat',backgroundSize: 'cover', backgroundPosition:'center center'}} >
              </div>
         
              <div className="col-md-6">
                  <div className="login d-flex align-items-center py-5">
                      <Container className="container">
                          <Row className="row">
                              <div className="col-lg-10 col-xl-7 mx-auto">
                                  <h3 className="display-4">CUSTOMER LOGIN</h3>
                                
                                  <Form className="formBody" onSubmit={e=>authenticate(e)}>      
                                      <Form.Group as={Row} controlId="email"> 
                                       
                                          <Form.Control className="form-control rounded-pill border-0 shadow-sm px-4 text-secondary" type="email" placeholder="Insert Email Here" onChange={event=>setEmail(event.target.value)} value={email} required></Form.Control>
                                       
                                      </Form.Group>

                                      <Form.Group as={Row} controlId="password"> 
                                    
                                            <Form.Control className="form-control rounded-pill border-0 shadow-sm px-4 text-secondary" type="password" placeholder="Insert Password Here" value={password} onChange={event=>setPassword(event.target.value)} required></Form.Control>
                                              
                                      </Form.Group>
                                     
                                      
                                      {isActive ?
                                        <Button type="submit" id="submitBtn" variant="warning" className="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">LOGIN</Button>
                                        :
                                        <Button type="submit" id="submitBtn" variant="danger" disabled className="bbtn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">LOGIN</Button>
                                      }
                                     
                                          <Nav.Link className="" style={{color: 'white', textDecoration: 'underline'}}as={NavLink} to="/register">New Customer? Sign Up Here!</Nav.Link>
                                      
                                  </Form>

                              </div>
                          </Row>
                      </Container>

                  </div>
              </div>

          </div>
      </div>

    
  
  )
}
