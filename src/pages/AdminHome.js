
import Hero from '../components/Banner'
import Showcase from '../components/Highlights'
import {Carousel,Container,Nav} from 'react-bootstrap'
import {NavLink} from 'react-router-dom';

let details = {
	title: "About Us",
	tagline: "La Vida Store is a lifestyle brand creating products that merge aesthetic and function.  Our vision is deeply rooted in giving recognition to the creativity and skill of local craftsmanship and prioritizing sustainable and eco-friendly products."
}


export default function Home(){
	return(


	
		<div className="adminhome py-5">
		  	<div className="p-5"> </div>
		  	<div></div>
		  <div className="row">
		
		    <div className="col-lg-6 mb-3 mb-lg-0">
		      <div className="hover hover-1 text-white rounded"><img src="catalog5.jpg" alt=""/>
		        <div className="hover-overlay"></div>
		        <div className="hover-1-content px-5 py-4">
		          <h3 className="hover-1-title text-uppercase font-weight-bold mb-0">
		          <Nav.Link style={{color: '#fff8e7', textDecoration: 'none'}} as={NavLink} to="/adminretrieve">View Catalog</Nav.Link>
		          </h3>
		          <p className="hover-1-description font-weight-light mb-0">This page features all store items. Be able deactivate, activate and display all available and out of stock items.</p>
		          
		        </div>
		      </div>
		    </div>
		
		    <div className="col-lg-6">
		      <div className="hover hover-1 text-white rounded"><img src="catalog3.jpg" alt=""/>
		        <div className="hover-overlay"></div>
		        <div className="hover-1-content px-5 py-4">
		          <h3 className="hover-1-title text-uppercase font-weight-bold mb-0">
		          <Nav.Link style={{color: '#fff8e7', textDecoration: 'none'}} as={NavLink} to="/add">Create Product</Nav.Link>
		          </h3>
		          <p className="hover-1-description font-weight-light mb-0">This page allows admin to create product to be added to the in store items. Be able to add products to La Vida Wear</p>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
	
	
	);
}


