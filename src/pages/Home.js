
import Hero from '../components/Banner'
import Showcase from '../components/Highlights'
import {Carousel,Container} from 'react-bootstrap'


let details = {
	title: "About Us",
	tagline: "La Vida Store is a lifestyle brand creating products that merge aesthetic and function.  Our vision is deeply rooted in giving recognition to the creativity and skill of local craftsmanship and prioritizing sustainable and eco-friendly products."
}


export default function Home(){
	return(
		<div className="home">
				<Carousel fade={false} slide={false} className="homeCarousel">
				  <Carousel.Item className="carouselItem" src="BrandShoot1.jpg"
				      alt="First slide">
				     <img
				      className="carouselItem d-block w-100" 
				      src="BrandShoot1.jpg"
				      alt="First slide"
				    />
				    
				  </Carousel.Item>
				  <Carousel.Item className="carouselItem"  src="BrandShoot2.jpeg"
				      alt="Second slide">
				    <img
				      className="carouselItem d-block w-100"
				      src="BrandShoot2.jpeg"
				      alt="Second slide"
				    />

				   
				  </Carousel.Item>
				  <Carousel.Item className="carouselItem" src="BrandShoot3.jpeg"
				      alt="Third slide">
				     <img
				      className="carouselItem d-block w-100"
				      src="BrandShoot3.jpeg"
				      alt="Third slide"
				    />
				   
				  </Carousel.Item>
				</Carousel>
			
			</div>
	
	);
}


