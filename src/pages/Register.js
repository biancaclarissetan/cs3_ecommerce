
import {useState,useEffect,useRef} from 'react';
import {Form,Button,Container,Row,Nav,Card,Col,Overlay,Popover} from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2'
import {Redirect, useHistory, NavLink} from 'react-router-dom';

const paraSaBanner ={
  title:"Create Your Own Account Here",
  tagline: "Think before you Click"
}


export default function Register(){
  const history = useHistory();
  const [show, setShow] = useState(false);
  const [target, setTarget] = useState(null);
  const ref = useRef(null);
  const [firstName, setFirstName] = useState('');
  const [middleName, setMiddleName] = useState('');
  const [lastName, setLastName]= useState('');
  const [userEmail, setUserEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [mobileNo, setmobileNo]= useState('');

  const [isRegisterBtnActive, setRegisterBtnActive] = useState('');
  const [isComplete, setIsComplete] = useState(false);
  const [isPasswordSame, setIsPasswordSame] = useState(false);
  const [isMobileNoValid,setIsMobileNoValid] = useState(false);

  const [passwordLength, setPasswordLength] = useState(false);
  const [passwordNumber, setPasswordNumber] = useState(false);
  const [passwordSpecialCharacter, setPasswordSpecialCharacter] = useState(false);
  const [passwordLowercase, setPasswordLowercase] = useState(false);
  const [passwordUppercase, setPasswordUppercase] = useState(false);
  const [passwordContainsFirstName, setPasswordContainsFirstName] = useState(false);


  var passwordAllConditions = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
  var passwordLengthCondition= new RegExp("^(?=.{8,})");
  var passwordNumberCondition=  new RegExp("^(?=.*[0-9])");
  var passwordSpecialCharacterCondition= new RegExp("^(?=.*[!@#\$%\^&\*])");
  var passwordLowercaseCondition= new RegExp("^(?=.*[a-z])");
  var passwordUppercaseCondition=  new RegExp("^(?=.*[A-Z])");

  const handleClick = (event) => {
      setShow(!show);
      setTarget(event.target);
    };

  useEffect(()=>{

        //MOBILE NUMBER VALIDATION (USED FOR REACTIVE COMPONENTS)
        let evalMobileNo= mobileNo.slice(0,4)
        var validNum=["0933","0934","0935","0936","0937","0938","0939","0940","0941","0942","0943","0945","0946","0947","0948","0949","0950","0951","0953","0954","0955","0956","0966","0967"]

        let validNumChecker=validNum.includes(evalMobileNo)

        if(validNumChecker===true&&mobileNo.length===11){
          setIsMobileNoValid(true);

        }else{
          setIsMobileNoValid(false);
        }

          //PASSWORD VALIDATION (USED FOR REACTIVE COMPONENTS)
          if(passwordLengthCondition.test(password1)){
            setPasswordLength(true)
          }else{
            setPasswordLength(false)
          }
          if(passwordNumberCondition.test(password1)){
            setPasswordNumber(true)
          }else {
            setPasswordNumber(false)
          }

          if(passwordSpecialCharacterCondition.test(password1)){
            setPasswordSpecialCharacter(true);
          }else{
            setPasswordSpecialCharacter(false)
          }

          if(passwordLowercaseCondition.test(password1)){
            setPasswordLowercase(true);
          }else{
            setPasswordLowercase(false)
          }

          if(passwordUppercaseCondition.test(password1)){
            setPasswordUppercase(true);
          }else{
            setPasswordUppercase(false);
          }

          //Check if password uses username/firstname 
            let convertedFirstName = firstName.toLowerCase()
            let firstNameSplit = convertedFirstName.split(" ")


            let isIncludes = firstNameSplit.some(format=>password1.includes(format))
            
              if(isIncludes){
                setPasswordContainsFirstName(false)
              }else{
                setPasswordContainsFirstName(true)
              }
            
              if(password1===""){
                setPasswordContainsFirstName(false)
              }



    //Overall condition to submit           
    if(password1===password2 && password1!==''){
       
      if (firstName!==''&&middleName!==''&&lastName!==''&&userEmail!==''&&password1!==''&&password2!==''&&validNumChecker===true&&mobileNo.length===11&&passwordAllConditions.test(password1)&&!isIncludes) {
        setRegisterBtnActive(true);
        setIsComplete(true);
        setIsPasswordSame(true);
      } else {
        setRegisterBtnActive(false);
        setIsComplete(false);
        setIsPasswordSame(true);  
      }
    }else{

      setIsPasswordSame(false);
      setRegisterBtnActive(false);
      setIsComplete(false);
    }
  },[firstName, middleName, lastName, userEmail, password1, password2, mobileNo]);
  

  function registerUser(event){
    event.preventDefault();

    

    fetch(`https://stark-meadow-66994.herokuapp.com/users/register`,{
      method:"POST",headers:{
        'Content-Type':'application/json'
      },body: JSON.stringify({
        firstName: firstName,
        middleName: middleName,
        lastName: lastName,
        email: userEmail,
        password: password1,
        mobileNo: mobileNo
      })
    })
    .then(res=> res.json())
    .then(data=>{
      console.log(data)

        if(data){
          Swal.fire({
          title: `Hey ${firstName}, You have Registered Succcesfully`,
          icon: 'success',
          text: 'Your New Account Has been Created'
          });
          history.push('/login')
        }else{
          Swal.fire({
          title: `Registered Failed`,
          icon: 'error',
          text: 'Failed Registration. Email has already been taken'
          });
        }
    
        
    })
  }



  return( 

    <div className="registerpage container-fluid">
          <div className="row no-gutter">      
              <div className="col-md-6 d-none d-md-flex bg-image" style={{backgroundImage: `url(catalog5.jpg)`, backgroundRepeat: 'no-repeat', backgroundSize: 'cover', backgroundPosition:'center center'}} >
              </div>
         
              <div className="col-md-6">
                  <div className="login d-flex align-items-center py-5">
                      <Container className="container">
                          <Row className="row">
                              <div className="col-lg-10 col-xl-7 mx-auto">
                                  <h6 className="display-4">CREATE ACCOUNT</h6>
                                
                                  
                                  <Form className="formBody"  onSubmit={(event)=>registerUser(event)}>


                                      <Form.Group as={Row}  controlId="firstName">  
                                            <Form.Control className="rounded-pill border-0 shadow-sm px-4 text-secondary" type="text" placeholder="Insert First Name Here" value={firstName} onChange={event=>setFirstName(event.target.value)} required>
                                            </Form.Control>
                                      </Form.Group>

                                      <Form.Group as={Row} controlId="middleName"> 
                                          <Form.Control type="text" placeholder="Insert Middle Name Here" value={middleName} onChange={event=>setMiddleName(event.target.value)}required className="rounded-pill border-0 shadow-sm px-4 text-secondary">
                                          </Form.Control>
                                      </Form.Group>
                                              
                                      <Form.Group as={Row} controlId="lastName"> 
                                          <Form.Control type="text" placeholder="Insert Last Name Here" value={lastName} onChange={event=>setLastName(event.target.value)} required className="rounded-pill border-0 shadow-sm px-4 text-secondary">
                                          </Form.Control>
                                      </Form.Group>
                                      
                                      <Form.Group as={Row} controlId="userEmail"> 
                                          <Form.Control type="email" placeholder="Insert Email Here" onChange={event=>setUserEmail(event.target.value)} value={userEmail} required className="rounded-pill border-0 shadow-sm px-4 text-secondary">
                                          </Form.Control>
                                      </Form.Group>

                                      <Form.Group as={Row} controlId="password1"> 
                                        <div className="input-group ">
                                         {/* <Form.Control type="text" className="form-control" className="rounded-pill border-0 shadow-sm px-4 text-secondary" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2"> 
                                          </Form.Control>*/}
                                          <Form.Control className="rounded-pill border-0 shadow-sm px-4 text-secondary" type="password" placeholder="Insert Password Here" value={password1} onChange={event=>setPassword1(event.target.value)} onInput = {(e) =>{e.target.value =  e.target.value.slice(0,16)}} required></Form.Control>

                                          {/*<div className="input-group-append">
                                            <button className="btn btn-outline-secondary" className="rounded-left border-0 shadow-sm ml-2 px-0" type="button">Button</button>
                                          </div>*/}
                                          <div ref={ref}>
                                                <Button className="btn btn-outline-white rounded-left border-0 shadow-sm ml-2 px-0" variant="danger" onClick={handleClick}>info</Button>

                                                <Overlay
                                                  show={show}
                                                  target={target}
                                                  placement="bottom"
                                                  container={ref.current}
                                                  containerPadding={20}
                                                >
                                                  <Popover id="popover-contained">
                                                    <Popover.Title as="h3">Password Requirements</Popover.Title>
                                                    <Popover.Content>
                                                      <strong></strong>

                                                       {passwordLength?
                                                         <small id="passwordHelpBlock" className="form-text text-success">
                                                         8-16 characters long
                                                         </small>
                                                       :
                                                         <small id="passwordHelpBlock" className="form-text text-muted">
                                                         8-16 characters long
                                                         </small>
                                                       }

                                                       {passwordNumber?
                                                         <small id="passwordHelpBlock" className="form-text text-success">
                                                         contain at least 1 number
                                                         </small>
                                                       :
                                                         <small id="passwordHelpBlock" className="form-text text-muted">
                                                         contain at least 1 number
                                                         </small>
                                                       }

                                                       {passwordSpecialCharacter?
                                                         <small id="passwordHelpBlock" className="form-text text-success">
                                                         contain at least 1 special character (limited to !@#\$%\^&\*)
                                                         </small>
                                                       :
                                                         <small id="passwordHelpBlock" className="form-text text-muted">
                                                         contain at least 1 special character (limited to !@#\$%\^&\*)
                                                         </small>
                                                       }

                                                       {passwordLowercase?
                                                         <small id="passwordHelpBlock" className="form-text text-success">
                                                         contain at least 1 lowercase alphabetical character
                                                         </small>
                                                       :
                                                         <small id="passwordHelpBlock" className="form-text text-muted">
                                                         contain at least 1 lowercase alphabetical character
                                                         </small>
                                                       }

                                                       {passwordUppercase?
                                                         <small id="passwordHelpBlock" className="form-text text-success">
                                                          contain at least 1 uppercase alphabetical character
                                                         </small>
                                                       :
                                                         <small id="passwordHelpBlock" className="form-text text-muted">
                                                          contain at least 1 uppercase alphabetical character
                                                         </small>
                                                       }

                                                       {passwordContainsFirstName?
                                                         <small id="passwordHelpBlock" className="form-text text-success">
                                                          must not contain first name
                                                         </small>
                                                       :
                                                         <small id="passwordHelpBlock" className="form-text text-muted">
                                                          must not contain first name
                                                         </small>
                                                       }
                                                    </Popover.Content>
                                                  </Popover>
                                                </Overlay>
                                              </div>
                                        </div>
                                       
                           {/*
                                          <Col sm="9">
                                            <Form.Control type="password" placeholder="Insert Password Here" value={password1} onChange={event=>setPassword1(event.target.value)} onInput = {(e) =>{e.target.value =  e.target.value.slice(0,16)}} required></Form.Control>
                                             <small id="passwordHelpBlock" className="form-text text-muted">
                                              Password Requirements:
                                             </small>
                                              {passwordLength?
                                                <small id="passwordHelpBlock" className="form-text text-success">
                                                8-16 characters long
                                                </small>
                                              :
                                                <small id="passwordHelpBlock" className="form-text text-muted">
                                                8-16 characters long
                                                </small>
                                              }

                                              {passwordNumber?
                                                <small id="passwordHelpBlock" className="form-text text-success">
                                                contain at least 1 number
                                                </small>
                                              :
                                                <small id="passwordHelpBlock" className="form-text text-muted">
                                                contain at least 1 number
                                                </small>
                                              }

                                              {passwordSpecialCharacter?
                                                <small id="passwordHelpBlock" className="form-text text-success">
                                                contain at least 1 special character (limited to !@#\$%\^&\*)
                                                </small>
                                              :
                                                <small id="passwordHelpBlock" className="form-text text-muted">
                                                contain at least 1 special character (limited to !@#\$%\^&\*)
                                                </small>
                                              }

                                              {passwordLowercase?
                                                <small id="passwordHelpBlock" className="form-text text-success">
                                                contain at least 1 lowercase alphabetical character
                                                </small>
                                              :
                                                <small id="passwordHelpBlock" className="form-text text-muted">
                                                contain at least 1 lowercase alphabetical character
                                                </small>
                                              }

                                              {passwordUppercase?
                                                <small id="passwordHelpBlock" className="form-text text-success">
                                                 contain at least 1 uppercase alphabetical character
                                                </small>
                                              :
                                                <small id="passwordHelpBlock" className="form-text text-muted">
                                                 contain at least 1 uppercase alphabetical character
                                                </small>
                                              }

                                              {passwordContainsFirstName?
                                                <small id="passwordHelpBlock" className="form-text text-success">
                                                 must not contain first name
                                                </small>
                                              :
                                                <small id="passwordHelpBlock" className="form-text text-muted">
                                                 must not contain first name
                                                </small>
                                              }

                                          </Col>*/}
                                        
                                      </Form.Group>

                                     
                                     

                                      <Form.Group as={Row} controlId="password2"> 
                                          <Form.Control type="password" placeholder="Confirm Password Here" value={password2} onChange={event=>setPassword2(event.target.value)} onInput = {(e) =>{e.target.value =  e.target.value.slice(0,16)}} required className="rounded-pill border-0 shadow-sm px-4 text-secondary">
                                          </Form.Control>
                                              {isPasswordSame? 
                                                  <small className="form-text text-success"> *Passwords Match*</small>
                                              :
                                                   <small className="form-text text-danger"> *Passwords Should Match*</small>     
                                              }
                                      </Form.Group>

                                      <Form.Group as={Row} controlId="mobileNo"> 

                                            <Form.Control  className="rounded-pill border-0 shadow-sm px-4 text-secondary" type="number" placeholder="Insert Mobile Number Here" value={mobileNo} onChange={event=>setmobileNo(event.target.value)} onInput = {(e) =>{e.target.value =  e.target.value.slice(0,11)}} required></Form.Control>
                                            {isMobileNoValid?  
                                              <small  className="text-success">*Mobile Is Valid*</small>
                                            :
                                              <small  className="text-danger">*Mobile Number must be Valid*</small> 
                                            }
                                      </Form.Group>

                                      {isRegisterBtnActive ?
                                      <Button type="submit" id="submitBtn" variant="warning" className="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">CREATE ACCOUNT</Button>
                                      :
                                      <Button type="submit" id="submitBtn" variant="danger" className="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm"disabled>CREATE ACCOUNT</Button>
                                      }

                                      <Nav.Link  style={{color: 'white', textDecoration: 'underline'}}as={NavLink} to="/login">Already Have an Account? Login Here</Nav.Link>
                                  </Form>
                              </div>
                          </Row>
                      </Container>

                  </div>
              </div>

          </div>
      </div>
    
    
  
  )
}

/*
<div className="registerBody">
      <Container className="page-wrapper p-t-5">
          <div className="wrapper wrapper--w790">
              <Card className="registercard card-5">
                  <Card.Title className="card-heading p-3">
                      <h2 className="title">Create Account</h2>
                  </Card.Title>
                  <Card.Body className="card-body">

                      <Form className="formBody"  onSubmit={(event)=>registerUser(event)}>


                          <Form.Group as={Row} className="mb-3" controlId="firstName">  
                              <Form.Label  className="col-sm-3"  >First Name: </Form.Label>
                              <Col sm="9">
                                <Form.Control type="text" placeholder="Insert First Name Here" value={firstName} onChange={event=>setFirstName(event.target.value)} required></Form.Control>
                              </Col>
                          </Form.Group>

                          <Form.Group as={Row} controlId="middleName"> 
                            <Form.Label   className="col-sm-3" >Middle Name: </Form.Label>
                            <Col sm="9">
                              <Form.Control type="text" placeholder="Insert Middle Name Here" value={middleName} onChange={event=>setMiddleName(event.target.value)}required></Form.Control>
                            </Col>
                          </Form.Group>
                                  
                          <Form.Group as={Row} controlId="lastName"> 
                            <Form.Label   className="col-sm-3" >Last Name *</Form.Label>
                             <Col sm="9">
                              <Form.Control type="text" placeholder="Insert Last Name Here" value={lastName} onChange={event=>setLastName(event.target.value)} required></Form.Control>
                             </Col>
                          </Form.Group>
                          
                          <Form.Group as={Row} controlId="userEmail"> 
                            <Form.Label   className="col-sm-3" >Email Address *</Form.Label>
                            <Col sm="9">
                              <Form.Control type="email" placeholder="Insert Email Here" onChange={event=>setUserEmail(event.target.value)} value={userEmail} required></Form.Control>
                            </Col>
                          </Form.Group>

                          <Form.Group as={Row} controlId="password1"> 
                            <Form.Label   className="col-sm-3" >Password *</Form.Label>
                              <Col sm="9">
                                <Form.Control type="password" placeholder="Insert Password Here" value={password1} onChange={event=>setPassword1(event.target.value)} onInput = {(e) =>{e.target.value =  e.target.value.slice(0,16)}} required></Form.Control>
                                 <small id="passwordHelpBlock" className="form-text text-muted">
                                  Password Requirements:
                                 </small>
                                  {passwordLength?
                                    <small id="passwordHelpBlock" className="form-text text-success">
                                    8-16 characters long
                                    </small>
                                  :
                                    <small id="passwordHelpBlock" className="form-text text-muted">
                                    8-16 characters long
                                    </small>
                                  }

                                  {passwordNumber?
                                    <small id="passwordHelpBlock" className="form-text text-success">
                                    contain at least 1 number
                                    </small>
                                  :
                                    <small id="passwordHelpBlock" className="form-text text-muted">
                                    contain at least 1 number
                                    </small>
                                  }

                                  {passwordSpecialCharacter?
                                    <small id="passwordHelpBlock" className="form-text text-success">
                                    contain at least 1 special character (limited to !@#\$%\^&\*)
                                    </small>
                                  :
                                    <small id="passwordHelpBlock" className="form-text text-muted">
                                    contain at least 1 special character (limited to !@#\$%\^&\*)
                                    </small>
                                  }

                                  {passwordLowercase?
                                    <small id="passwordHelpBlock" className="form-text text-success">
                                    contain at least 1 lowercase alphabetical character
                                    </small>
                                  :
                                    <small id="passwordHelpBlock" className="form-text text-muted">
                                    contain at least 1 lowercase alphabetical character
                                    </small>
                                  }

                                  {passwordUppercase?
                                    <small id="passwordHelpBlock" className="form-text text-success">
                                     contain at least 1 uppercase alphabetical character
                                    </small>
                                  :
                                    <small id="passwordHelpBlock" className="form-text text-muted">
                                     contain at least 1 uppercase alphabetical character
                                    </small>
                                  }

                                  {passwordContainsFirstName?
                                    <small id="passwordHelpBlock" className="form-text text-success">
                                     must not contain first name
                                    </small>
                                  :
                                    <small id="passwordHelpBlock" className="form-text text-muted">
                                     must not contain first name
                                    </small>
                                  }
                              </Col>
                            
                          </Form.Group>
                         
                          <Form.Group as={Row} controlId="password2"> 
                            <Form.Label   className="col-sm-3" >Confirm Password: </Form.Label>
                           
                            <Col sm="9">
                              <Form.Control type="password" placeholder="Confirm Password Here" value={password2} onChange={event=>setPassword2(event.target.value)} onInput = {(e) =>{e.target.value =  e.target.value.slice(0,16)}} required></Form.Control>
                                  {isPasswordSame? 
                                      <small className="form-text text-success"> *Passwords Match*</small>
                                  :
                                       <small className="form-text text-danger"> *Passwords Should Match*</small>     
                                  }
                               
                                
                            </Col>
                            
                          </Form.Group>

                          <Form.Group as={Row} controlId="mobileNo"> 
                            <Form.Label    className="col-sm-3">Mobile Number:</Form.Label>   
                            <Col sm="9">
                                <Form.Control   type="number" placeholder="Insert Mobile Number Here" value={mobileNo} onChange={event=>setmobileNo(event.target.value)} onInput = {(e) =>{e.target.value =  e.target.value.slice(0,11)}} required></Form.Control>
                                {isMobileNoValid?  
                                  <small  className="text-success">*Mobile Is Valid*</small>
                                :
                                  <small  className="text-danger">*Mobile Number must be Valid*</small> 
                                }
                            </Col>
                          </Form.Group>

                          {isRegisterBtnActive ?
                          <Button type="submit" id="submitBtn" variant="warning" className="btn">CREATE ACCOUNT</Button>
                          :
                          <Button type="submit" id="submitBtn" variant="danger" className="btn" disabled>CREATE ACCOUNT</Button>
                          }

                          <Nav.Link  style={{color: 'white', textDecoration: 'underline'}}as={NavLink} to="/login">Already Have an Account? Login Here</Nav.Link>
                      </Form>
                   
                  </Card.Body>
              </Card>
            
              
          </div>
      </Container>
      
    </div>
   
*/