//the purpose of this new module is to allow us to create a context object to declare a state about the status of the user/client for our entire application.
import React from 'react'; 

//now from the react library acquire the createContext() function in order to declare/create a context object for the entire app.
  //the context that we will try to declare will describe the status of the USER/CLIENT.
     //2 POSSIBLE STATUS:
       //1. AUTHENTICATED/log in 
       //2. UNauthenticated/log out.
    //unlike the states that declared before. THIS STATE ABOUT THE USER will be declared on a "global" scale for the application.
const UserContext = React.createContext(); //a context object will be CREATED.
//this variable will identify the status of the user in our application.
//understood why is is going to be easier to describe the state/status of the user in a global scale rather than identifying the status of the user on each page individually?

//we need the 2nd component in creating a context object. PROVIDER component
//The "Provider" component is the responsible in allowing/feeding information or data to the subscribers/consumer component.
export const UserProvider = UserContext.Provider; //the provider property of the context object for the user. now we will repackage it inside a new variable so that we can easily identify it. 
//We need to "expose" this provider component to other document/modules inside the components tree
//Provider => supplies

//we also need to expose the Context object for the user so that the consumers/subscribers can easily feed on the data provided in the context object 
//the documents/modules that will take in the "context" object for the user will serve as the consumer
//Consume => acquires/needs/uses the data supplied by the provider.
export default UserContext; 
