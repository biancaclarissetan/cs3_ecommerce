import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'
ReactDOM.render(

    <App />
  ,
  document.getElementById('root')
);

//lets declare a variable called name

/*let name = "Bianca";*/
//lets create an element that will allow is to store and display the "name" variable with an heading property

//lets create an object that will allow us to store information about a certain subject

/*let user = {
	firstName : 'Bianca',
	lastName : 'Tan',
	age: '23'
}

//lets create a function using the es6 syntax
	const formatName = (userInfo) => {
		return `My Name is ${userInfo.firstName}, I am ${userInfo.age} years old.`
	}

//lets place the html script/tag inside a new variable

const HTMLelement = <h1>Hello, {formatName(user)} !</h1>
//display the output of the function inside the browser page

//lets try to display this element inside the browser.
	//document => describes the document where the modules is attached to
	//getElementById() => DOM method we are trying to use in order to target and capture the HTML document

	//in order to use the render() of react. you need to identify the collection/library that defines the source of the function
	ReactDOM.render(
		HTMLelement,
	  	document.getElementById("root")
	);
	*/