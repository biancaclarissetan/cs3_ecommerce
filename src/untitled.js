
import {useState,useEffect} from 'react';
import {Form,Button,Container,Row,Nav,Card,Col} from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2'
import {Redirect, useHistory, NavLink} from 'react-router-dom';

const paraSaBanner ={
  title:"Create Your Own Account Here",
  tagline: "Think before you Click"
}


export default function Register(){
  const history = useHistory();
  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice]= useState('');
  const [orderId, setOrderId] = useState('');
 
  useEffect(()=>{

    



    
  },[/*productName,description,price,orderId,*/);
  

  function registerProduct(event){
    event.preventDefault();

    

    fetch(`https://stark-meadow-66994.herokuapp.com/products/register`,{
      method:"POST",headers:{
        'Content-Type':'application/json'
      },body: JSON.stringify({
        productName: productName
        description: description,
        price: price,
        orderId: orderId
      })
    })
    .then(res=> res.json())
    .then(data=>{
      console.log(data)

        if(data){
          Swal.fire({
          title: `Hey ${productName}, You have Registered Succcesfully`,
          icon: 'success',
          text: 'Your New Account Has been Created'
          });
          history.push('/login')
        }else{
          Swal.fire({
          title: `Registered Failed`,
          icon: 'error',
          text: 'Failed Registration. Email has already been taken'
          });
        }
    
        
    })
  }



  return(
    <div className="registerBody">
      <Container className="page-wrapper p-t-5">
          <div className="wrapper wrapper--w790">
              <Card className="card card-5">
                  <Card.Title className="card-heading p-3">
                      <h2 className="title">Create Account</h2>
                  </Card.Title>
                  <Card.Body className="card-body">

                      <Form className="formBody"  onSubmit={(event)=>registerProduct(event)}>


                          <Form.Group as={Row} className="mb-3" controlId="productName">  
                              <Form.Label  className="col-sm-3"  >Product Name: </Form.Label>
                              <Col sm="9">
                                <Form.Control type="text" placeholder="Insert Product Name Here" value={productName} onChange={event=>setProductName(event.target.value)} required></Form.Control>
                              </Col>
                          </Form.Group>

                          <Form.Group as={Row} controlId="description"> 
                            <Form.Label   className="col-sm-3" >Description: </Form.Label>
                            <Col sm="9">
                              <Form.Control type="text" placeholder="Insert Product Descripton Here" value={description} onChange={event=>setDescription(event.target.value)}required></Form.Control>
                            </Col>
                          </Form.Group>
                                  
                          <Form.Group as={Row} controlId="lastName"> 
                            <Form.Label   className="col-sm-3" >Price: </Form.Label>
                             <Col sm="9">
                              <Form.Control type="text" placeholder="Insert Product Price" value={price} onChange={event=>setPrice(event.target.value)} required></Form.Control>
                             </Col>
                          </Form.Group>
                          

                          {isRegisterBtnActive ?
                          <Button type="submit" id="submitBtn" variant="warning" className="btn">CREATE ACCOUNT</Button>
                          :
                          <Button type="submit" id="submitBtn" variant="danger" className="btn" disabled>CREATE ACCOUNT</Button>
                          }

                          <Nav.Link  style={{color: 'white', textDecoration: 'underline'}}as={NavLink} to="/login">Already Have an Account? Login Here</Nav.Link>
                      </Form>
                   
                  </Card.Body>
              </Card>
            
              
          </div>
      </Container>
      
    </div>
   

    
  
  )
}
