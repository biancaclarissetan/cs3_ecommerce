import './App.css';

import Navbar from './components/AppNavbar';
import Footer from './components/Footer';
import Home from './pages/Home'
import Products from './pages/Catalog';
import Login from './pages/Login';
import Register from './pages/Register';
import SingleProduct from './pages/SingleProduct';
import Error from './pages/Error';
import {UserProvider} from './UserContext'
import {useState,useEffect} from 'react';
import Logout from './pages/Logout'
import Add from './pages/AdminCreateProduct'
import {Route,Switch,BrowserRouter as Router}from 'react-router-dom'
import AdminRetrieve from './pages/AdminRetrieveAllProduct'
import AdminHome from './pages/AdminHome'
			


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  

 
  const unsetUser = () => {
     
     localStorage.clear(); 

     setUser({
        id: null,
        isAdmin: null
     })
  }


  useEffect(()=>{

    fetch(`https://stark-meadow-66994.herokuapp.com/users/details`,{
        headers:{
        Authorization: `Bearer ${localStorage.getItem('acessToken')}`
         }
        }).then(resultOfPromise => resultOfPromise.json()).then(convertedResult=>{
           
            if (convertedResult._id !== "undefined") {
              setUser({
                id: convertedResult._id,
                isAdmin: convertedResult.isAdmin
              })
            } else {
              setUser({
                id: null,
                isAdmin: null
              })
            }    
          })

  },[])

  return (
    <UserProvider value={{user, unsetUser, setUser}}>
      <Router>
        <Navbar/>

        <Switch> 
            <Route exact path="/" component={Home}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/register" component={Register}/> 
            <Route exact path="/shop" component={Products}/> 
             <Route exact path="/logout" component={Logout}/> 
             <Route exact path="/add" component={Add}/>
             <Route exact path="/adminretrieve" component={AdminRetrieve}/>
              <Route exact path="/adminhome" component={AdminHome}/>
           

            <Route exact path="/:id" component={SingleProduct}/>
             <Route component={Error}/>  
            
        </Switch>
        
      
       <Footer/>
      </Router>
    </UserProvider>
  );
}

export default App;
