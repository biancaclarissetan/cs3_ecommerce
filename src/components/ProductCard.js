
import Card from 'react-bootstrap/Card';
import {Row,Col,Nav,Button} from 'react-bootstrap'

import {NavLink} from 'react-router-dom'

export default function ProductCard({infoNgKurso}){


const {_id,productName, description, price} =infoNgKurso;

	return(	

						  
						     <div className="col-lg-3 col-md-6 mb-4 mb-lg-0 mt-4">
						       <Card className="productcard rounded shadow-sm border-0">
						         <div className="card-body p-4">
						         <img src={`${_id}.jpg`} alt="" className="img-fluid d-block mx-auto mb-3"/>
						           <h5> <a href={`${_id}`} className="text-dark">{productName}</a></h5>
						           <p className="small text-muted font-italic">{description}</p>
						           {/*
						           		{ <Nav.Link style={{color: '#75654C', textDecoration: 'underline'}} as={NavLink} to={`${_id}`}> <img className="img-responsive" src={_id + '.jpg'}/> 
						           		 </Nav.Link>}
						           	  */}
						          
						           	    {/*<Button type="submit" id="submitBtn" variant="white" className="btn btn-block">
						           	    	Fave
						           	    </Button>*/}
						         </div>
						       </Card>
						     </div>
						   
						
				
			


	);
}

