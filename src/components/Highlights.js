import {Row, Col, Card} from 'react-bootstrap'
	



export default function Highlights(){
	return(
		<Row className= "highlightsbody mt-3 mb-5">
			
			<Col className="mb-3" xs={12} md={4}>
				<Card className="Highlights p-3">
					<Card.Body>	
						<Card.Title>
							Learn From Home
						</Card.Title>
						<Card.Text>	
							Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae provident, adipisci cumque iure debitis. Magni temporibus tempore aperiam, officia consequatur at rerum voluptatem vitae doloribus.	
						</Card.Text>
					</Card.Body>		
				</Card>
			</Col>
			
			<Col className="mb-3" xs={12} md={4}>
				<Card className="Highlights p-3">
					<Card.Body>	
						<Card.Title>
							Study Now, Pay Later
						</Card.Title>
						<Card.Text>	
							Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae provident, adipisci cumque iure debitis. Magni temporibus tempore aperiam, officia consequatur at rerum voluptatem vitae doloribus.	
						</Card.Text>
					</Card.Body>		
				</Card>
			</Col>
			
			<Col className="mb-3" xs={12} md={4}>
				<Card className="Highlights p-3">
					<Card.Body>	
						<Card.Title>
							Be Part of Our Community
						</Card.Title>
						<Card.Text>	
							Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae provident, adipisci cumque iure debitis. Magni temporibus tempore aperiam, officia consequatur at rerum voluptatem vitae doloribus.	
						</Card.Text>
					</Card.Body>		
				</Card>	
			</Col>
		</Row>
	);	
}


//this will serve as a feature section/catalog section of our application which we can use to advertise what our app/organization can do