

import {Nav,Navbar,Container,Row,Col} from 'react-bootstrap'
import {useContext, Fragment} from 'react';
import UserContext from '../UserContext';
import {NavLink} from 'react-router-dom'



function AppNavbar(){
  
  const {user} = useContext(UserContext);

   return( 
    <Navbar expand="sm" className="navbar-expand-xl fixed-top">
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
        <Navbar.Collapse id="basic-navbar-nav">
            
                
               
                {(user.id)?
                (user.isAdmin===true)?
                //USER ADMIN
                  <Fragment>
                   <Nav.Link style={{color: '#fff8e7', textDecoration: 'none'}} as={NavLink} to="/adminhome">Home</Nav.Link>
                   <Nav.Link  style={{color: '#fff8e7', textDecoration: 'none'}}as={NavLink} to="/add">Register Product</Nav.Link>
                   <Nav.Link  style={{color: '#fff8e7', textDecoration: 'none'}}as={NavLink} to="/adminretrieve">Catalog</Nav.Link>
                  <Nav.Link style={{color: '#fff8e7', textDecoration: 'none'}} as={NavLink} to="/logout">Logout</Nav.Link>
                  </Fragment>
                
                :
                //USER SHOPPER
                  <Fragment>
                   <Nav.Link style={{color: '#fff8e7', textDecoration: 'none'}} as={NavLink} to="/">Home</Nav.Link>
                   <Nav.Link  style={{color: '#fff8e7', textDecoration: 'none'}}as={NavLink} to="/shop">SHOP</Nav.Link>
                   <Nav.Link style={{color: '#fff8e7', textDecoration: 'none'}} as={NavLink} to="/logout">Logout</Nav.Link>
                 </Fragment>
                ://WALA NOT LOGGED IN MGA BASIC LNG

                <Fragment>
                  <Nav.Link style={{color: '#fff8e7', textDecoration: 'none'}} as={NavLink} to="/">Home</Nav.Link>
                  <Nav.Link  style={{color: '#fff8e7', textDecoration: 'none'}}as={NavLink} to="/shop">SHOP</Nav.Link>
                  <Nav.Link style={{color: '#fff8e7', textDecoration: 'none'}} as={NavLink} to="/register">Register</Nav.Link>
                  <Nav.Link style={{color: '#fff8e7', textDecoration: 'none'}} as={NavLink} to="/login">Login</Nav.Link>
   
                </Fragment>
                
               
                }  


            
        </Navbar.Collapse>
    </Navbar>   

    
   )
}

export default AppNavbar;






/**/