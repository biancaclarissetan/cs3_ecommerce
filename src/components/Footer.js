
import {Card,Col,Row} from 'react-bootstrap'


function Footer() {
  return(

    <div className="d-flex flex-column h-100">   
        <footer className="w-100 py-4 flex-shrink-0">
            <div className="container py-4">
                <div className="row gy-4 gx-5">
                    <div className="col-lg-4 col-md-6">
                        <h5 className="h1 text-white">La Vida Wear</h5>
                        <p className="small text-white">Viva La Vida, here's to living life to the fullest, wearing your everyday life wear</p>
                        <p className="small text-white mb-0">&copy; Copyrights. Bianca Tan</p>
                    </div>
                    <div className="col-lg-2 col-md-6">
                        
                    </div>
                    <div className="col-lg-2 col-md-6">
                        
                    </div>
                    <div className="col-lg-4 col-md-6">
                        <h5 className="text-white mb-3">Newsletter</h5>
                        <p className="small text-white">Subscribe to our Newsletter to keep updated with our lifestyle promos and deals!</p>
                        <form action="#">
                            <div className="input-group mb-3">
                                <input className="form-control" type="text" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2"/>
                                <button className="btn btn-primary" id="button-addon2" type="button"><i className="fas fa-paper-plane"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </footer>
    </div>
  )
};



export default Footer;
