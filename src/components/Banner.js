
import {Col,Row,Container} from 'react-bootstrap';


function Banner({kahitAno}){

	const {title, tagline} = kahitAno;
	return(
		<Container>
			<Row className="banner">
				<Col className="p-5">
					<h2>{title}</h2>
					<p width="1px">{tagline}</p>
				</Col>
			</Row>
		</Container>
		

	)
}

export default Banner; 
