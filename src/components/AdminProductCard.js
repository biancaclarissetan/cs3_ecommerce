
import Card from 'react-bootstrap/Card';
import {Row,Col,Nav,Button} from 'react-bootstrap'
import {useEffect,useState} from 'react';
import {NavLink} from 'react-router-dom'
import Swal from 'sweetalert2'
export default function AdminProductCard({infoNgKurso}){


const {_id,productName, description, price,isActive} =infoNgKurso;
const [archive, setArchive] = useState([]);
const [unarchive, setUnarchive] = useState([]);
const archiveProduct = (e) =>{
	  e.preventDefault()

	  fetch(`https://stark-meadow-66994.herokuapp.com/products/archiveProduct/${_id}`,{
	    method:"PUT",headers:{
	      'Content-Type':'application/json',
	      Authorization: `Bearer ${localStorage.getItem('acessToken')}`
	    }
	  })
	  .then(res=>res.json())
	  .then(information=>{

	    if(information===true){
	      localStorage.setArchive('acessToken', information.acessToken)
	      Swal.fire({
	        title: "Product Activated" ,
	        icon: "error"
	      })
	     
	    }else{
	      Swal.fire({
	        title: "Product Deactivated" ,
	        icon: "success"
	        
	      })
	    }
	  })
	}


const unarchiveProduct = (e) =>{
	  e.preventDefault()

	  fetch(`https://stark-meadow-66994.herokuapp.com/products/unarchiveProduct/${_id}`,{
	    method:"PUT",headers:{
	      'Content-Type':'application/json',
	      Authorization: `Bearer ${localStorage.getItem('acessToken')}`
	    }
	  })
	  .then(res=>res.json())
	  .then(information=>{
	
	    if(information===true){
	      localStorage.setUnarchive('acessToken', information.acessToken)
	      Swal.fire({
	        title: "Product Deactivated" ,
	        icon: "error"
	      })
	     
	    }else{
	      Swal.fire({
	        title: "Product Activated" ,
	        icon: "success"
	        
	      })
	    }
	  })
	}


	return(	

						  
						     <div className="col-lg-3 col-md-6 mb-4 mb-lg-0 mt-4">
						       <Card className="productcard rounded shadow-sm border-0">
						         <div className="card-body p-4">
						         <img src={`${_id}.jpg`} alt="" className="img-fluid d-block mx-auto mb-3"/>
						           <h5> <a href={`${_id}`} className="text-dark">{productName}</a></h5>
						           
						           <p className="small text-muted font-italic">{description}</p>

						           {(isActive===true)?
						           	<div>
						           			<Button disabled variant="success" className="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">
						           		    	Available
						           		    </Button>
						           		<Button type="submit" onClick={e=>archiveProduct(e)} id="submitBtn" variant="danger" className="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">
						           	    	Deactivate Product
						           	    </Button>
						           	</div>
						          

						           	:

						           	 <div>
						           	 		<Button disabled variant="danger" className="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">
						           	 	    	Not Available
						           	 	    </Button>
						           	 	<Button  type="submit" id="submitBtn" onClick={e=>unarchiveProduct(e)} variant="success" className="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">
						           	    	Activate Product
						           	    </Button>
						           	 </div>
						          	 
						       		}
						           {/*
						           		{ <Nav.Link style={{color: '#75654C', textDecoration: 'underline'}} as={NavLink} to={`${_id}`}> <img className="img-responsive" src={_id + '.jpg'}/> 
						           		 </Nav.Link>}
						           	  */}
						          
						           	    
						         </div>
						       </Card>
						     </div>
						   
						
				
			


	);
}

